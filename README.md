﻿# 1. Opis problemu i sformułowanie zadania

Zaprojektować i zaimplementować (w dowolnym języku i środowisku) aplikację programową, która dla zadanej dokładności obliczeń i wskazanego zbioru danych, zawierającego 240 odstępów czasowych pomiędzy wykryciem kolejnych błędów, umożliwia wyznaczenie wartości estymatorów parametrów N, 𝜙 następujących modeli:

❑ Jelińskiego-Morandy,

❑ Schicka-Wolvertona.

Wykorzystując wyznaczone wartości parametrów N, 𝜙 dla każdego z ww. modeli obliczyć wartość oczekiwaną czasu, jaki upłynie do momentu wykrycia kolejnego (241.) błędu.

**Model Jelińskiego-Morandy**

W procesie testowania programu wykryto n błędów, przy czym wielkości t1, t2, t3, ..., tn oznaczają długości przedziałów czasu pomiędzy wykryciem kolejnych błędów. Przyjmując, że funkcja intensywności występowania błędów jest postaci:

![jelinski-moranda1.png](images/jelinski-moranda1.png)

można pokazać, że w oparciu o metodę największej wiarygodności estymatory MLE (Maximum Likelihood Esitmation) parametrów Ф oraz N wyznacza się z zależności:

![jelinski-moranda2.png](images/jelinski-moranda2.png)

Wartość oczekiwana czasu, jaki upłynie do momentu wykrycia kolejnego (241.) błędu:

![jelinski-moranda3.png](images/jelinski-moranda3.png)

**Model Schicka-Wolvertona**

W procesie testowania programu wykryto n błędów, przy czym wielkości t1, t2, t3, ..., tn oznaczają długości przedziałów czasu pomiędzy wykryciem kolejnych błędów. Przyjmując, że funkcja intensywności występowania błędów jest postaci:

![schick-Wolverton.png](images/schick-Wolverton.png)

można pokazać, że w oparciu o metodę największej wiarygodności estymatory MLE (Maximum Likelihood Esitmation) parametrów Ф oraz N wyznacza się z zależności:

![schick-Wolverton2.png](images/schick-Wolverton2.png)

Ze wzoru:
![schick-Wolverton3.png](images/schick-Wolverton3.png)
można wyznaczyć Φ i wtedy przyjmie ono postać:
![schick-Wolverton4.png](images/schick-Wolverton4.png)

Wartość oczekiwana czasu, jaki upłynie do momentu wykrycia kolejnego (241.) błędu:
![schick-Wolverton5.png](images/schick-Wolverton5.png)

# 2. Specyfikacja wymagań

Wymagania funkcjonalne przedstawione są w tabeli poniżej:

| Nazwa wymagania | Treść wymagania |
| :-------------: | :-------------: |
| wprowadzenie dokładności obliczeń | Aplikacja musi umożliwić wprowadzenie dokładności obliczeń (epsilon) z dokładnością do liczb zmiennoprzecinkowych |
| Wybranie pliku z danymi | Aplikacja musi umożliwić wybranie pliku z poziomu okna dialogowego przedstawiającego pliki znajdujące się w systemie |
| Sprawdzenie czy dokładność obliczeń została wprowadzona oraz czy plik został wgrany | Aplikacja powinna sprawdzać, czy dokładność obliczeń została wprowadzona oraz czy plik z danymi został wybrany oraz uniemożliwić uruchomienie programu dopóki parametry nie zostały wprowadzone |
| Sprawdzenie rozszerzenia pliku | Aplikacja powinna umożliwiać odczytanie pliku tylko z rozszerzeniem .txt |
| Wyznaczenie oraz wyświetlenie parametrów N, Ф, wartości oczekiwanej dla modeli Jelińskiego-Morandy oraz Schicka-Wolvertona | Aplikacja powinna wyznaczać oraz wyświetlać parametry N, Ф, wartości oczekiwanej dla modeli Jelińskiego-Morandy oraz Schicka-Wolvertona |

# 3. Specyfikacje projektowe

Aplikacja zostanie przygotowana z uwzględnieniem graficznego interfejsu użytkownika. Interfejs umożliwi:

- wybranie pliku z danymi
- wprowadzenie dokładności obliczeń
- uruchomienie wykonywania obliczeń
- wyświetlenie wyników obliczeń

Przyjmuje się, że plik z danymi powinien być odpowiednio sformatowany tzn. powinny w nim się znajdować liczby naturalne oddzielone dowolnym znakiem nie będącym cyfrą. Ponadto przyjmuje się, że kolejne liczby oznaczają odstępy pomiędzy wykryciem kolejnych błędów. Poniższa tabela przedstawia przykładowy, poprawnie sformatowany plik z czasami w minutach pomiędzy wykryciem kolejnych błędów (wartości czytane są wierszami):
![dane_przyklad.JPG](images/dane_przyklad.JPG)

Przyjmuje się, że format liczby oznaczającej dokładność obliczeń będzie następujący:

- maksymalnie 7 cyfr przed separatorem dziesiętnym, jeden separator dziesiętny, maksymalnie 10 cyfr po separatorze dziesiętnym

Obliczenia parametrów N, Ф, wartości oczekiwanej dla modeli Jelińskiego-Morandy oraz Schicka-Wolvertona zostaną wykonane na polecenie użytkownika na akcję przycisku.

Wyniki obliczeń zostaną wyświetlone w postaci zbiorczej w jednym polu tekstowym dla wszystkich modeli. Początkowo pole tekstowe będzie puste.

# 4. Opis implementacji (w tym opis wykorzystanych technik, technologii i narzędzi)

Aplikacja została napisana przy wykorzystaniu języka Java 8, a także z wykorzystaniem biblioteki JavaFx umożliwiającej tworzenie interfejsu graficznego.

W programie zostały użyte takie mechanizmy jak strumienie oraz wyrażenia Lambda charakterystyczne dla języka Java 8.

Struktura projektu przypomina strukturę projektów Maven.

Elementy graficzne zostały zaprogramowane z wykorzystaniem znaczników xml i umieszczone w oddzielnym pliku gui.fxml

Klasą odpowiedzialną za sterowanie elementami graficznymi jest klasa GuiController.

Za zarządzanie plikami jest odpowiedzialna klasa FileManager. To tutaj odbywa się przechwycenie zawartości pliku.

W klasie EstimationParametersObject umieszczone są wszystkie parametry oczekiwane jako wynik uruchomienia programu tzn. wartość N, Ф, wartość oczekiwana oraz nazwa modelu.

Wspólnymi elementami dla obydwu modeli: Jelińskiego-Morandy oraz Schicka-Wolvertona są: dokładność obliczeń, zawartość pliku z danymi, metoda obliczająca wszystkie parametry obiektu EstimationParametersObject oraz metoda obliczająca wartość oczekiwaną. Elementy te zostały wydzielone do klasy abstrakcyjnej EstimationModel.

Klasy JelinskiMoranda oraz SchickWolverton to klasy odpowiedzialne za wykonanie konkretnych obliczeń w celu uzyskania wartości obiektu EstimationParametersObject.

Częstą operacją wykonywaną w ramach obliczeń w klasach JelinskiMoranda oraz SchickWolverton jest operacja sumowania. Metoda odpowiedzialna za wykonywanie sumowania została wydzielona do oddzielnej klasy MathematicalOperation. Metoda przyjmuje argumenty:

- „from” – oznacza liczbę od jakiej zaczynamy sumowanie
- „to” – oznacza liczbę na jakie kończymy sumowanie
- "function" – oznacza interfejs funkcyjny pozwalający na wprowadzenie dowolnej funkcji przyjmującej pojedynczy argument, który przyjmuje wartość początkową „from”, zwiększa się o 1 z każdym obrotem pętli, natomiast na końcu osiąga wartość „to”
# 5. Opis wyników testowania, w tym na programach udostępnionych przez prowadzącego.

Aplikacja została przetestowana na zbiorze danych dostarczonym przez prowadzącego. Jest to plik dane.txt. Poniższa tabela przedstawia zawartość pliku:
![dane_przyklad.JPG](images/dane_przyklad.JPG)

Aplikacja została przetestowana z różnymi wartościami dla parametru dokładność obliczeń. Wyniki są następujące:

a) Dla dokładności obliczeń epsilon=0.001
![ep=0.001.JPG](images/ep=0.001.JPG)
b) Dla dokładności obliczeń epsilon=0.0001
![ep=0.0001.JPG](images/ep=0.0001.JPG)
c) Dla dokładności obliczeń epsilon=0.00001
![ep=0.00001.JPG](images/ep=0.00001.JPG)
# 6. Opis instalacji aplikacji

Aby uruchomić aplikację należy uruchomić plik „maxLikelihoodEstimation.jar”. Powinien ukazać się następujący ekran:
![aplikacja.JPG](images/aplikacja.JPG)
Aby wgrać plik z danymi należy wcisnąć przycisk „open” i wybrać plik. Aby wprowadzić wartość epsilon należy wpisać liczbę zmiennoprzecinkową. Po podaniu powyższych parametrów przycisk „run” zostanie aktywowany. Jego wciśnięcie spowoduje uruchomienie wykonywania obliczeń i spowoduje wyświetlenie wyników w polu tekstowym pod napisem „Wyniki”.
