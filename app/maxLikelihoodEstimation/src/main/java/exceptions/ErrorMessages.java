package exceptions;

public class ErrorMessages {
    public static final String EMPTY_EPSILON = "Proszę uzupełnić wartość epsilon";
    public static final String INCORRECT_FILE_CONTENT = "Niepoprawny plik z danymi. Proszę wgrać nowy plik";
    public static final String FILE_NOT_CHOSEN = "Proszę wybrać plik z danymi";
}
