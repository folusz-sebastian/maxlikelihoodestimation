package fileManager;

import exceptions.ErrorMessages;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class FileManager {
    private final File file;
    private final List<Integer> data;

    public FileManager(String path) {
        file = new File(path);
        data = new LinkedList<>();
    }

    public boolean readData() {
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextInt()) {
                data.add(scanner.nextInt());
            }
        } catch (FileNotFoundException e) {
            System.err.println(ErrorMessages.INCORRECT_FILE_CONTENT);
            return false;
        }
        return true;
    }

    public List<Integer> getData() {
        return data;
    }
}
