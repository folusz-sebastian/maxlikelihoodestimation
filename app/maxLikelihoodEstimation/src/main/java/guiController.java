import exceptions.ErrorMessages;
import fileManager.FileManager;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import model.EstimationModel;
import model.EstimationParametersObject;
import model.JelinskiMoranda;
import model.SchickWolverton;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class guiController {

    @FXML
    public TextField epsilonValue;
    @FXML
    public Label errorMsg;
    @FXML
    private Button openBtn;
    @FXML
    private TextArea resultsTextArea;
    @FXML
    private Button runBtn;

    private File selectedFile;

    @FXML
    private void initialize() {
        runBtn.setDisable(true);
        errorMsg.setVisible(false);

        epsilonValue.setOnKeyReleased(keyEvent -> checkIfProgramCanBeRun());

        openBtn.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();

            FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("TEXT files (*.txt)","*.txt");
            fileChooser.getExtensionFilters().add(extensionFilter);
            selectedFile = fileChooser.showOpenDialog(null);
            checkIfProgramCanBeRun();
        });

        runBtn.setOnAction(event -> {
            FileManager fileManager = new FileManager(selectedFile.getAbsolutePath());
            if (fileManager.readData()) {
                String results = executeProgram(Double.parseDouble(epsilonValue.getText()), fileManager.getData());
                resultsTextArea.setText(results);
            }
        });

        acceptOnlyNumbersInEpsilonTextField();
    }

    private void checkIfProgramCanBeRun() {
        if (epsilonValueIsNotSet()) {
            runBtn.setDisable(true);
            showErrorMsg(ErrorMessages.EMPTY_EPSILON);
        }
        else if (selectedFile == null) {
            showErrorMsg(ErrorMessages.FILE_NOT_CHOSEN);
        } else {
            runBtn.setDisable(false);
            errorMsg.setVisible(false);
        }
    }

    private String executeProgram(double epsilon, List<Integer> data) {
        List<EstimationModel> models = Arrays.asList(
                new JelinskiMoranda(data, epsilon), new SchickWolverton(data, epsilon)
        );

        return models.stream()
                .map(EstimationModel::findEstimationParameters)
                .map(EstimationParametersObject::toString)
                .collect(Collectors.joining());
    }

    private void showErrorMsg(String message) {
        errorMsg.setText(message);
        errorMsg.setVisible(true);
    }

    private boolean epsilonValueIsNotSet() {
        return epsilonValue.getText().equals("") ||
                epsilonValue.getText().equals("0") ||
                epsilonValue.getText().equals("0.");
    }

    private void acceptOnlyNumbersInEpsilonTextField() {
        epsilonValue.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d{0,7}([\\.]\\d{0,20})?")) {
                epsilonValue.setText(oldValue);
            }
        });
    }
}
