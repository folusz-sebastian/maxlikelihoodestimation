package model;

import java.util.List;
import static mathematicalOperations.MathematicalOperation.sum;

public class JelinskiMoranda extends EstimationModel {
    private final double sumOfDataElements;

    public JelinskiMoranda(List<Integer> data, double epsilon) {
        super(data, epsilon);
        sumOfDataElements = sum(0, data.size()-1, data::get);
    }

    @Override
    public EstimationParametersObject findEstimationParameters() {
        int n = data.size();
        double fi;
        double error;
        do {
            n++;
            double finalN = n;
            double firstFactor = sum(1, data.size(), i -> 1.0 / (finalN - (i - 1)));
            fi = countFi(n);
            error = firstFactor - fi * sumOfDataElements;
        } while(error > epsilon);
        return new EstimationParametersObject("JelinskiMoranda", n, fi, countExpectedValue(n, fi));
    }

    public double countFi(double n) {
        double firstFactor = n * sumOfDataElements;
        double secondFactor = sum(1, data.size(), i -> (i - 1)*data.get(i-1));
        return data.size() / (firstFactor - secondFactor);
    }

    @Override
    public double countExpectedValue(double n, double fi) {
        return 1.0 / (fi * (n - (data.size() + 1)));
    }
}
