package model;

public class EstimationParametersObject {
    String modelName;
    int n;
    double fi;
    double expectedValue;

    public EstimationParametersObject(String modelName, int n, double fi, double expectedValue) {
        this.modelName = modelName;
        this.n = n;
        this.fi = fi;
        this.expectedValue = expectedValue;
    }

    public double getExpectedValue() {
        return expectedValue;
    }

    public void setExpectedValue(double expectedValue) {
        this.expectedValue = expectedValue;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    public double getFi() {
        return fi;
    }

    public void setFi(double fi) {
        this.fi = fi;
    }

    @Override
    public String toString() {
        return String.format("EstimationParameters for %s:%n", modelName) +
                String.format("N = %d%n", n) +
                String.format("Fi = %.15f%n", fi) +
                String.format("Expected value = %f%n%n", expectedValue);
    }
}
