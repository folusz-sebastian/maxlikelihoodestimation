package model;

import java.util.List;

public abstract class EstimationModel {
    protected final List<Integer> data;
    protected final double epsilon;

    public EstimationModel(List<Integer> data, double epsilon) {
        this.data = data;
        this.epsilon = epsilon;
    }

    public abstract EstimationParametersObject findEstimationParameters();
    protected abstract double countExpectedValue(double n, double fi);
}
