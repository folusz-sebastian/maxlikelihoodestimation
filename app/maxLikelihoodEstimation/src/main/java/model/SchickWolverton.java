package model;

import java.util.List;
import static mathematicalOperations.MathematicalOperation.sum;

public class SchickWolverton extends EstimationModel {
    private final double sumOfSquaredElements;

    public SchickWolverton(List<Integer> data, double epsilon) {
        super(data, epsilon);
        this.sumOfSquaredElements = sum(0, data.size()-1, i -> Math.pow(data.get(i),2));
    }

    @Override
    public EstimationParametersObject findEstimationParameters() {
        int n = data.size();
        double error;
        double fi;
        do{
            n++;
            fi = countFi(n);
            error = Math.abs(countFi(n) - countFiSecondMethod(n));
        } while (error > epsilon);
        return new EstimationParametersObject("SchickWolverton", n, fi, countExpectedValue(n, fi));
    }

    public double countFi(double n) {
        return 2 * sum(1, data.size(), i-> {
            double firstFactor = n - (i-1);
            return 1/(firstFactor*sumOfSquaredElements);
        });
    }

    public double countFiSecondMethod(double n) {
        double firstFactor = 2*data.size();
        double secondFactor = sumOfSquaredElements*n;
        double thirdFactor = sum(1, data.size(), i -> (i-1)*Math.pow(data.get(i-1),2));
        return firstFactor / (secondFactor - thirdFactor);
    }

    @Override
    public double countExpectedValue(double n, double fi) {
        double firstFactor = 2 * fi;
        double secondFactor = n - (data.size() + 1);
        return Math.sqrt(Math.PI / (firstFactor * secondFactor));
    }
}
