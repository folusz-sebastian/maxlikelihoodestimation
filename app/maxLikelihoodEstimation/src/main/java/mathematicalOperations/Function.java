package mathematicalOperations;

@FunctionalInterface
public interface Function {
    public double functionWithVariable(int i);
}
