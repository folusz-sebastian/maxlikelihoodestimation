package mathematicalOperations;

public class MathematicalOperation {
    public static double sum(int from, int to, Function function) {
        double result = 0.0;
        for (int i = from; i <= to; i++) {
            result += function.functionWithVariable(i);
        }
        return result;
    }
}
